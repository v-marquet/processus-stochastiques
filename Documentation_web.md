# Partie 1: intro à PageRank

* [blog de wassner](http://wassner.blogspot.fr/2014/06/pourquoi-est-ce-important-la-plupart.html)
    * trop vulgarisé par rapport à ce qu'on nous demande niveau math


# Partie 2: math

* [Université de Stuttgart](http://www.igt.uni-stuttgart.de/eiserm/enseignement/google-promenade.pdf)
    * Le côté mathématique est clairement abordé, chaînes de Markov (page 3), tout ça...

* [Explication du PageRank et propriétés de celle-ci](http://guillaume.segu.in/papers/pagerank.pdf)

* [Université de Genève](http://cui.unige.ch/tcs/cours/algoweb/2008/documents/linkanalysis.pdf)
    * vocabulaire: marche aléatoire (page 35), matrice de transition (page 36)...

* [Quelques aspects des chaînes de Markov](http://djalil.chafai.net/docs/biskra-markov.pdf)
    * plus technique, lien avec les chaînes de Markov (page 53)
