\documentclass[12pt,a4paper]{article}


% Importation des packages nécessaires à la génération PDF
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{eurosym}
\usepackage{hyperref}
\usepackage[colorinlistoftodos]{todonotes}
% \usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{comment}
% \usepackage{bbold}
\hypersetup{
	colorlinks=true,
	linkcolor=black,
	urlcolor=blue,
	pdfborder={0 0 0},
	breaklinks=true
}
\usepackage{listings}
% colors
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
% listings parameters
\lstset{frame=tb,
  language=Ruby,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{black},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}




% Page de garde
\title{Projet de Processus Stochastiques \\ L'algorithme PageRank}
\date{}
\author{
	Vincent Marquet \and
	Mérivan Hassan \and
	Parth Kalvindkar \and
	Yann Privé
}



% Le bandeau en haut de chaque page
\pagestyle{fancyplain} \chead{}\lhead{\textit{Processus Stochastiques}} \rhead{\emph{\textit{L'algorithme PageRank}}}

\begin{document}

\maketitle

% si on doit mettre le logo de l'école:
%\begin{figure}[hp]
%	\centering
%	\includegraphics[width=0.60\textwidth]{logo.png}
%	\label{fig:logo}
%\end{figure}

% Image d'illustration PageRank
\begin{figure}[hp]
	\centering
	\includegraphics[width=0.80\textwidth]{Images_rapport/PageRank.png}
	\label{fig:logo}
\end{figure}

\begin{abstract}
L'objectif de ce projet est de comprendre le fonctionnement de l'algorithme PageRank créé initialement par Google.
\end{abstract}



% Sommaire
\newpage
\tableofcontents
\newpage



% Introduction sur l'algo PageRank (i.e. blabla pour remplir le rapport)
\section*{Introduction}
\addcontentsline{toc}{section}{Introduction} 
Du fait, que les premières pages sur Internet n'étaient pas classées, il fallait inventer un algorithme permettant d'offrir aux utilisateurs des résultats plus pertinents.
Ainsi, PageRank est un algorithme permettant l'analyse des relations de liens entre les pages web. \\

Créé par Larry Page, il a une importance capitale dans le classement de pages lors des recherches sur Google entre autres.
Dans ce projet, notre groupe va expliquer le fonctionnement de cet algorithme par le biais de calculs mathématiques et de simulations.



% Partie I : Présentation générale de l'algorithme PageRank : son intérêt, son fonctionnement.
\newpage
\section{Présentation générale de l'algorithme PageRank}

\subsection{Pertinence d'une page web}
L'objectif du PageRank est de donner à chaque page web une note en fonction de la pertinence de son contenu.
Déterminer cette pertinence est quelque chose de délicat car il y a plusieurs considérations possibles.
La solution retenue dans le PageRank est de considérer qu'une page fortement citée est alors considérable comme référence. \\

Plusieurs modèles sont envisageables pour définir cette pertinence, modèles qui sont alors plus ou moins complexes mathématiquement.
Dans la suite nous développerons sur les avantages et inconvénients de plusieurs modèles pour ensuite développer sur l'algorithme PageRank employé par Google. \\

La valeur que prend le PageRank est comprise entre 0 et $10^{10}$ mais est en pratique ramenée entre 0 et 10 par échelle logarithmique pour plus de clarté pour l'utilisateur.


\subsection{Le web est un graphe}
En considérant l'ensemble du web, nous constatons que l'ensemble des intéractions entre les pages forme un graphe orienté. \\

En effet, par le biais des liens hypertextes, une page I va pointer vers une page J, page qui va elle-même pointer vers une page K.
Ainsi, nous avons bien un graphe orienté, sur lequel surfe de manière plus ou moins aléatoire l'internaute.

Plusieurs types de liens sont envisageables. Considérons le cas de deux pages I et J.
\begin{itemize}
	\item La page I peut pointer vers la page J et inversement.
	\item La page I et J peuvent pointer mutuellement l'une sur l'autre.
	\item La page I ou J peut pointer sur elle-même.
\end{itemize}

\begin{figure}[!h]
	\center
	\includegraphics[width=0.75\textwidth]{Images_rapport/web_est_graphe.png}
	\caption{Représentation de relations entre I et J}
\end{figure}

\vspace{1\baselineskip}

En ce sens, nous observons alors l'idée d'une chaîne de Markov sur laquelle se déplace l'utilisateur.
Cela sera développé dans la partie suivante.

\subsection{L'internaute est représentable par un vecteur}
Lorsqu'un internaute est sur une page web donnée I, les liens affichés par I lui offrent la possibilité de s'échapper vers une autre page J.
La probabilité qu'il aille sur cette page J est inversement proportionnelle au nombre de liens présents sur la page I. \\

Ainsi, le calcul du PageRank va se baser sur la répétition de cette expérience aléatoire où un internaute est sur une page donnée et part sur une autre page par le biais d'un lien.
Nous comprenons assez rapidement que plus une page est citée, plus la probabilité de présence de l'internaute sur cette page est forte et donc plus celle-ci aura un fort PageRank.

\subsection{Le PageRank, un élément parmis d'autres}
Cette technologie, créée et adoptée par Google, n'est que la "partie visible" de son fonctionnement car chaque recherche est beaucoup plus complexe.
En effet, par le biais de l'utilisation de cookies, la connexion de l'utilisateur à son compte..., l'utilisateur est mieux connu et les résultats dépendent alors de son profil.



% Partie II : Lien entre l'algo et le cours de processus stochastiques : parler de chaîne de Markov, marche aléatoire...
\newpage
\section{Fonctionnement mathématique de l'algorithme PageRank}

\subsection{Notations et hypothèses}

Soit $n \in \mathbb{N}$. \\

Tout d'abord, notons $x_n$ la $n^{ieme}$ page de l'ensemble des pages que l'on veut classer avec l'algorithme PageRank.
Dans le vocabulaire des probabilités, on peut parler de l'ensemble des pages web étudiées comme d'un \textbf{ensemble d'éventualités}. \\

Ensuite, notons $X_n$ la page visitée à la $n^{ieme}$ étape (c'est-à-dire après avoir cliqué n fois sur un lien).
Mathématiquement, pour tout $ n \in \mathbb{N} $, $X_n$ est une \textbf{variable aléatoire} qui peut prendre n'importe quelle valeur dans l'ensemble des pages web étudiées.
La probabilité de se trouver à la page $x_n$ lors de l'étape $X_n$ se note $\mathbb{P}(X_n = x_n)$. \\

La famille de variables aléatoires $ (X_n)_{ n \in \mathbb{N} } $ est donc un \textbf{processus stochastique} à temps discret.


\subsection{Chaine de Markov}
De plus, la probabilité qu'à l'instant $n$, l'internaute arrive sur la page $x_n$, ne dépend que de la page sur laquelle il se trouve à l'instant $n-1$, puisqu'il change de page en cliquant aléatoirement sur un lien de la page sur laquelle il se trouve.
Mathématiquement, cela s'écrit:
\[ \mathbb{P}(X_n=x_n | X_{n-1}=x_{n-1}, ..., X_0=x_0) = \mathbb{P}(X_n=x_n | X_{n-1}=x_{n-1}) \]
Par définition, on en déduit donc que $ (X_n)_{ n \in \mathbb{N} } $ est une \textbf{chaîne de Markov}.


\subsection{Noyau de transition}
Le principe général de l'algorithme PageRank est donc de classer les différentes pages tel que la page la plus citée ait le meilleur classement.
Pour cela, chaque page pointée par une page donnée obtient un "poids" dont la distribution suit la loi normale. \\

Soit $n \in \mathbb{N}$.
Considérons un ensemble de pages $p_{i, i \in [\![0 ; n ]\!]}$.

Notons $P_{i,j}$ la probabilité que l'utilisateur clique sur la page j depuis la page i.

Notons $\mathbb{1}_{i,j}$ la fonction indicatrice tel que : \\
\[
\mathbb{1}_{i,j} = \left\{
	\begin{array}{ll}
		1 & \mbox{si la page i pointe vers la page j} \\
		0 & \mbox{sinon}
	\end{array}
\right.
\] \\

Soit $i \in [\![0 ; n ]\!]$ fixé.
Alors on a la relation :
\[
P_{i,j} = \frac{1}{\sum_{\substack{j \in [\![0;n]\!]}} \mathbb{1}_{i,j}}
\]

Nous constatons bien que la matrice $P = (P_{i,j})_{(i,j) \in [\![0 ; n ]\!]^2}$ que nous avons formée est une matrice stochastique car pour tout $i \in [\![0 ; n ]\!]$, on a la relation :
\[
\sum_{\substack{j \in [\![0;n]\!]}} P_{i,j} = 1
\]
Cette relation définit le noyau de transition de la chaîne de Markov que nous avons définie. \\

\subsection{Algorithme général}
L'algorithme du PageRank correspond donc à une marche aléatoire de l'internaute sur la matrice de transition précédente. \\

Comme précédemment, soit $n \in \mathbb{N}$ et $i \in [\![0 ; n ]\!]$ une page web fixée. \\
Notons $L_{\bullet \rightarrow i}$ l'ensemble des pages web pointant vers la page i et $N_j$ le nombre de liens sortants de la page j. \\

Dans un premier temps, nous pouvons définir la valeur du PageRank de la page i -- noté $P_R(i)$ -- récursivement comme étant définit par :
\[
P_R(i) = \sum_{\substack{j \in L_{\bullet \rightarrow i}}} \frac{P_R(j)}{N_j}
\]
Cependant, cette formule présente un inconvénient : la valeur du PageRank ne converge pas.

Ainsi, la formule du PageRank introduit-elle un coefficient d'amortissement -- noté $d$ -- permettant la convergence de la valeur du PageRank. \\

La formule devient alors :
\fbox{$
	\displaystyle
	P_R(i) = d \sum_{\substack{j \in L_{\bullet \rightarrow i}}} \frac{P_R(j)}{N_j} + (1-d)
$}


\subsection{Cas d'une page ne pointant vers aucune autre}
Un cas pathologique serait qu'une des pages ne pointe vers aucune autre.
C'est-à-dire le cas d'une page recevant un (ou plusieurs) liens mais d'où aucun lien ne sort.

\begin{figure}[!h]
	\center
	\includegraphics[width=0.60\textwidth]{Images_rapport/pas_lien_externe.png}
	\caption{Exemple où aucun lien ne sort (à droite)}
\end{figure}

Le problème que cela va poser vis-à-vis du calcul du PageRank est alors clairement identifiable : la page recevant les visiteurs d'autres pages va avoir un PageRank maximal.
En effet, elle correspond à un \textbf{état absorbant} de la chaîne de Markov ainsi construite. \\

La solution imaginée par Google a alors été de considérer que cette page pointe vers toutes les autres pages avec à chaque fois une probabilité $\frac{1}{n}$ où $n \in \mathbb{N}$ est le nombre de pages total. \\

Ainsi, dans le cas d'une page $i$ sans aucun lien sortant, la matrice de transition de la chaîne de Markov ainsi formée comporte à la ligne i remplie de coefficients $\frac{1}{n}$. \\


\subsection{Cas d'une boucle ou "Farm Link"}
Il était possible au début de faire accroître artificiellement le PageRank en créant ce qu'on appelle une "Farm Link".
Il s'agit en fait d'un ensemble de page pointant les une vers les autres, maintenant "captifs" les visiteurs au sein d'un ensemble de pages.
Mathématiquement, il s'agit d'un ensemble de quelques pages formant une \textbf{classe de communication} à elles seules. \\

Comme nous le voyons dans la figure ci-dessous, les 3 nœuds sur la droite de l'image communiquent enre eux et maintiennent "prisonniers" les internautes étant arrivés sur une de ces pages.

\begin{figure}[!h]
	\center
	\includegraphics[width=0.75\textwidth]{Images_rapport/farm_link.png}
	\caption{Représentation d'une Farm Link}
\end{figure}

Google est cependant en mesure de détecter cette technique et pénalise l'indexation des sites qui en ont recours.


% Partie III : Étude d'un cas particulier.
\newpage
\section{Étude d'un exemple}
Dans cette partie, nous allons étudier un graphe de transition qui modélise de façon minimaliste le Web.\\

Il est à noter que ce modèle représente des pages qui ont une même thématique : informatique, cuisine, licornes, ... \\

Le graphe de transition que nous allons étudier est le suivant :

\begin{figure}[!h]
	\center
	\includegraphics[width=0.60\textwidth]{Images_rapport/graphe2.png}
	\caption{Graphe de transition du web}
\end{figure}



\subsection{Explication des liens du graphe}


Dans notre graphe :
\begin{comment}
	\begin{figure}[!h]
		\center
		\includegraphics[width=0.60\textwidth]{Images_rapport/graphe.png}
		\caption{Graphe de transition explicatif}
	\end{figure}
\end{comment}

$0 \rightarrow 2$ par exemple sont des liens simples.

La page 0 possède un lien qui envoie vers 2. \\

$0 \longleftrightarrow 1$ est un lien double.

La page 0 possède un lien qui envoie l'internaute vers la page 1 

et inversement. \\

$7 \longleftrightarrow 7$ est un lien ou l'état boucle sur lui-même.

La page 7 possède un lien qui envoie un lien vers lui-même. \\

\subsection{Noyau de transition}
Une chaine de Markov est également définie par sa \textbf{loi initiale} (celle de $X_0$), et par un \textbf{noyau de transition}.
Dans notre exemple de graphe, on aurait donc la matrice de transition P définie par :


\[M =
\begin{pmatrix}
0 & 1/3 & 1/3 & 1/3 & 0 & 0 & 0 & 0 \\
1/3 & 0 & 1/3 & 0 & 1/3 & 0 & 0 & 0 \\
0 & 0 & 0 & 1/2 & 0 & 0 & 1/2 & 0 \\
1/2 & 0 & 0 & 1/2 & 0 & 0 & 0 & 0 \\
0 & 1/2 & 0 & 0 & 0 & 1/2 & 0 & 0 \\
0 & 1/2 & 0 & 0 & 1/2 & 0 & 0 & 0 \\
0 & 0 & 1/3 & 1/3 & 0 & 0 & 1/3 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 \\
%code d'une matrice en général
%  p_{0,0} & p_{0,1} & \cdots & p_{0,n} \\
%  p_{1,0} & p_{1,1} & \cdots & p_{1,n} \\
%  \vdots  & \vdots  & \ddots & \vdots  \\
%  p_{m,0} & p_{m,1} & \cdots & p_{m,n}
\end{pmatrix}\]
 \\

Cependant, comme nous l'avons expliqué dans la sous-partie 2.5, une page $i$ ne pointant vers aucune autre, ferait que dans le noyau de transition, à la ligne $i$, chacun des coefficients sera égal à $\frac{1}{n}$ avec $n$ le nombre de pages totales.\\

Ainsi en considérant la propriété de Google ci-dessus, on obtient la matrice de transition $P$ : \\

\[P =
\begin{pmatrix}
0 & 1/3 & 1/3 & 1/3 & 0 & 0 & 0 & 0 \\
1/3 & 0 & 1/3 & 0 & 1/3 & 0 & 0 & 0 \\
0 & 0 & 0 & 1/2 & 0 & 0 & 1/2 & 0 \\
1/2 & 0 & 0 & 1/2 & 0 & 0 & 0 & 0 \\
0 & 1/2 & 0 & 0 & 0 & 1/2 & 0 & 0 \\
0 & 1/2 & 0 & 0 & 1/2 & 0 & 0 & 0 \\
0 & 0 & 1/3 & 1/3 & 0 & 0 & 1/3 & 0 \\
1/8 & 1/8 & 1/8 & 1/8 & 1/8 & 1/8 & 1/8 & 1/8 \\
%code d'une matrice en général
%  p_{0,0} & p_{0,1} & \cdots & p_{0,n} \\
%  p_{1,0} & p_{1,1} & \cdots & p_{1,n} \\
%  \vdots  & \vdots  & \ddots & \vdots  \\
%  p_{m,0} & p_{m,1} & \cdots & p_{m,n}
\end{pmatrix}\]
 \\

Cette matrice $P$ a été obtenu en résolvant le système : \\
\[ 
	\begin{cases} 
	P_R(0) = d \sum_{\substack{j \in L_{\bullet \rightarrow 0}}} \frac{P_R(j)}{N_j} + (1-d) \\ 
	P_R(1) = d \sum_{\substack{j \in L_{\bullet \rightarrow 1}}} \frac{P_R(j)}{N_j} + (1-d) \\
	\vdots \\
	P_R(7) = d \sum_{\substack{j \in L_{\bullet \rightarrow 7}}} \frac{P_R(j)}{N_j} + (1-d) \\
	\end{cases}
\]



Avec $L_{\bullet \rightarrow i}$ l'ensemble des pages pointant vers i et $d$ un coefficient d'atténuation. \\

Dans la suite, on ne considèrera que \textbf{le noyau de transition $P$}. Ainsi on obtient les propriétés qui suivent ...

\subsection{Les classes de communication}
Toutes les pages communiquent entre elles.
Ainsi les classes de communication $ C_i $ sont :

\[ C_1\mbox{ = \{0,1,2,3,4,5,6,7\} } \]

Ainsi, le noyau est \textbf{irréductible}.

\subsection{Périodicité}
Du fait que la chaîne de Markov est irréductible, la période ne dépend pas de l'état.

Donc :
\[ d(x) = pgcd\{ n \geq 1; P^{n}(x,x)>0 \} \]

\vspace{1\baselineskip}

Choisissons la page 0 comme page de début, nous avons :

\begin{itemize}
	\item 0 $\rightarrow$ 1 $\rightarrow$ 0 : en 2 "coups"
	\item 0 $\rightarrow$ 1 $\rightarrow$ 2 $\rightarrow$ 3 $\rightarrow$ 0 : en 4 "coups"
	\item 0 $\rightarrow$ 1 $\rightarrow$ 4 $\rightarrow$ 5 $\rightarrow$ 1 $\rightarrow$ 0 : en 5 "coups"
\end{itemize}

\vspace{1\baselineskip}

Ainsi :
\[ d(0) = pgcd\{2, 4, 5\} = 1 \]

\vspace{1\baselineskip}

Donc la chaîne de Markov est \textbf{apériodique}.

Dans la réalité, nous observons bien ce phénomène : on peut revenir à une page $i$ donnée en passant par $n$ pages, 
avec $n \in \mathbb{N}$ un nombre fini. 


\subsection{Récurrence \& transcience}

Du fait que le noyau de transition est irréductible sur un espace d'état ici fini donc la chaîne de Markov est \textbf{récurrente positive}.

Notons que dans la réalité, le nombre de pages étant considéré comme presque infini, la récurrence est nulle au lieu d'être positive.

\subsection{Remarques}
Sachant que la chaîne de Markov dans notre exemple est apériodique, récurrente positive et irréductible, on pourrait appliquer le théorème Ergodique. Cependant dans la réalité, la chaîne de Markov ne vérifie pas une des hypothèses (du théorème Ergodique) qui est que la chaîne de Markov est récurrente positive.\\

Ainsi, grâce à ce modèle mathématique, nous avons montré les propriétés de PageRank ainsi que ce que pourrait être son algorithme.\\

\subsection{Simulation avec notre exemple}
Avec le programme que nous avons créé, nous obtenons la simulation de notre exemple ayant pour matrice de transition P :\\

\begin{figure}[h!]
	\centering
	\begin{minipage}[b]{0.45\linewidth}
		\includegraphics[width=\textwidth]{Images_rapport/Exemple_graphe.png}
		\caption{Graphe de transition de notre exemple}
		\label{fig:minipage1}
		\end{minipage}
		\quad
		\begin{minipage}[b]{0.45\linewidth}
		\includegraphics[width=\textwidth]{Images_rapport/Matrice.png}
		\caption{Matrice de transition de notre exemple}
		\label{fig:minipage2}
	\end{minipage}
\end{figure}

Nous observons que les coefficients de la matrice issue de notre programme concordent bien avec ceux de la matrice théorique $P$.


% Partie III : Simulation informatique.
\newpage
\section{Simulation par un programme CoffeeScript}

Afin d'illustrer le fonctionnement de l'algorithme PageRank, nous en avons réalisé une simulation interactive visible avec un navigateur web (récent) à l'adresse \href{http://vmarquet.github.io/pagerank/}{http://vmarquet.github.io/pagerank/}.


\subsection{Description du programme}

L'interface du programme se compose d'une zone d'affichage, et de quelques boutons pour des fonctionnalités additionnelles. \\

En cliquant sur la zone d'affichage, l'utilisateur peut créer des ronds, qui symbolisent des pages web. En reliant ces ronds par un glisser-déplacer, l'utilisateur peut créer des flèches entre ces ronds, qui symbolisent des liens hypertexte entre ces pages. \\

A chaque création de rond ou de flèche, le programme redéfinit automatiquement la matrice stochastique associé au système, avant de recalculer les coefficients du PageRank de chacune des pages. Le bouton "Display matrix" permet d'afficher la matrice stochastique du système. Quand au PageRank de chaque page, il est indiqué par la taille du rond associé à la page. 

\subsection{Algorithmes utilisés}

Le programme a été codé en \href{http://coffeescript.org/}{CoffeeScript}. Ce langage présente l'avantage d'être agréablement lisible, et de pouvoir faire tourner le programme dans un navigateur web. \\

\subsubsection{Les variables}

Avant de pouvoir détailler les algorithmes, nous devons introduire les noms de variables utilisés.

\begin{description}
	\item [node\_list]: une liste qui contient tous les ronds créés par l'utilisateur
	\item [link\_list]: une liste qui contiens tous les liens entre les ronds créés par l'utilisateur
	\item [matrix]: un tableau à deux dimensions que l'on va remplir après avoir calculé les coefficients de chaque case
	\item [userVector]: le vecteur dont la case numéro i correspondant à la probabilité que l'internaute se trouve sur la page i
\end{description}

\subsubsection{L'algorithme du remplissage de la matrice stochastique}

L'utilisateur définit des ronds, et des liens entre les ronds. Nous devons transformer cette information en une matrice avant de pouvoir ensuite calculer le PageRank.

\begin{lstlisting}
i = 0
while i < node_list.length

	# we count the number of links to another page, from this page
	n = 0
	for link in link_list
		if link.node_start is node_list[i]
			n++

	# we update coefficients for the current line in the matrix
	j = 0
	while j < node_list.length
		# if n = 0 (no link to another the page), user should not be locked on the page, so user is redirected on an other page (all pages with same probability)
		if n is 0
			if node_list.length is 1
				matrix[i][j] = 1
			else  # we only make links to another page, not to the same page
				if j is i
					matrix[i][j] = 0
				else
					matrix[i][j] = 1 / (node_list.length - 1)
		else
			# we search if the pages are linked
			linked = false
			for link in link_list
				if link.node_start is node_list[i] and link.node_end is node_list[j]
					matrix[i][j] = 1 / n
					linked = true
					break
			if linked is false
				matrix[i][j] = 0
		j++
	i++
\end{lstlisting}

\subsubsection{Le calcul du PageRank}

Maintenant que nous avons la matrice stochastique du système, il reste à calculer le PageRank. Tout d'abord, nous initialisons le vecteur userVector, tel qu'il a une probabilité équivalente d'être sur chacune des pages web.

\begin{lstlisting}
i = 0
while i < node_list.length
	userVector[i] = 1 / node_list.length
	i++
\end{lstlisting}

\vspace{1\baselineskip}

Ensuite, nous pouvons calculer le PageRank, pour cela, nous faisons plusieurs itérations de l'algorithme suivant, afin que userVector ait suffisamment convergé vers la solution finale :

\vspace{0.5\baselineskip}

\begin{lstlisting}
i = 0
userVectorNew = []  # the new value of userVector at the end of the iteration

# the matrix calculation: userVectorNew = userVector * matrix
while i < node_list.length
	userVectorNew[i] = 0
	j = 0
	while j < node_list.length
		userVectorNew[i] += userVector[j] * matrix[j][i]
		j++
	i++

userVector = userVectorNew
\end{lstlisting}

\vspace{1\baselineskip}

Ensuite, il ne reste plus qu'à affecter aux ronds une taille proportionnelle au PageRank de la page qu'ils représente. \\

L'intégralité des sources du programme est disponible à l'adresse: \\
 \href{https://github.com/vmarquet/pagerank}{https://github.com/vmarquet/pagerank}. \\

 La partie concernant l'algorithme PageRank est dans le fichier \href{https://github.com/vmarquet/pagerank/blob/gh-pages/pagerank.coffee}{pagerank.coffee}.


% Conclusion sur le projet. Évoquer la partie théorique et la simulation (Penser qu'il ne s'agit pas d'un exposé !)
\newpage
\section*{Conclusion}
\addcontentsline{toc}{section}{Conclusion}
L'algorithme PageRank de Google, comme il a été écrit par Larry Page à ses débuts, est finalement assez simple à comprendre. \\

Basé sur les chaînes de Markov, il permet de trier les résultats fournis à l'internaute de manière efficace en considérant qu'une page citée comme référence dans plusieurs articles mérite d'être dans le "top" des résultats de recherche. \\

Cet algorithme a continué d'être développé mais n'est pas divulgué par Google dans sa dernière version pour des raisons économiques.



% Page avec les sources / webographie (ou encore bibliographie comme le soulignait M. Bréhier)
\newpage
\section*{Sources}
\addcontentsline{toc}{section}{Sources}

\begin{itemize}
	\item Illustration de la page de garde : \url{http://commons.wikimedia.org/wiki/File:PageRank-hi-res.png}
	\item Document de référence pour la partie mathématique du PageRank : \url{http://guillaume.segu.in/papers/pagerank.pdf}
\end{itemize}

\end{document}
