#!/bin/bash

# if temporary dir doesn't exist, we create it
if [[ ! -d .tmp ]]; then
	mkdir .tmp
fi

# we convert from latex to pdf:
pdflatex -output-directory=.tmp Rapport.tex
mv .tmp/Rapport.pdf ./

exit 0
